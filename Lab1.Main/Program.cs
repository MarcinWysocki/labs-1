﻿using System;
using System.Collections.Generic;
using Lab1.Implementation;
using Lab1.Contract;
/*
 * 
    Zdefiniuj interfejs IBase oraz dwa interfejsy dziedziczące po nim: ISub1 i ISub2 (w folderze Contract) a także stwórz dwie różne klasy Impl1 i Impl2 implementujące odpowiednio ISub1 i ISub2 
 * (w folderze Implementation) z bezargumentowymi konstruktorami.
    
 * Utwórz generyczną kolekcję z kilkoma instancjami klas Impl1 i Impl2 w metodzie klasy Program (nazwa w polu collectionFactoryMethod, zwracany powinien być interfejs do kolekcji). 
 * Wykonaj na niej (przekazując ją jako argument) pewną operację w metodzie klasy Program (nazwa w polu collectionConsumerMethod) korzystając ze wspólnego interfejsu.
    Zdefiniuj interfejs IOther posiadający co najmniej jedną wspólną metodę (nazwa w polu otherCommonMethod) z interfejsem IBase zwracającą jakiś wynik oraz co najmniej jedną inną. 
 * 
 * Stwórz klasę Impl3 implementującą zarówno ISub1 jak i IOther. Pokaż wywołanie wspólnej operacji z obu nadrzędnych interfejsów we wszystkich możliwych kontekstach 
 * (podpowiedź: 3 wywołania dające inne rezultaty).

Konkretyzacja

    ISK2: IBase = ISsak, ISub1 = IKot, ISub2 = IPies, IOther = IRobot


 */
namespace Lab1.Main
{
    public class Program
    {
        public static ICollection<ISSak> Zwierzaki()
        {
            List<ISSak> Lista = new List<ISSak> { };

            Lista.Add(new Kot());
            Lista.Add(new Pies());
            Lista.Add(new KosmicznyKocur());

            return Lista;
        }

        public static void JakSieNazywasz(ICollection<ISSak> lista)
        {
            foreach (var item in lista)
            {
                Console.WriteLine(item.Nazwa());
            }
        }


        static void Main(string[] args)
        {

            JakSieNazywasz(Zwierzaki());
            Console.ReadKey();

        }
    }
}
