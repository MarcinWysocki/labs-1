﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Contract
{
    interface ISSak
	{
        string Nazwa();
        int LiczbaLap();
	}
	interface IPies:ISSak
    {
        string Szczekaj();
    }
	interface IKot : ISSak
    {
        string Miaucz();
    }
	interface IRobot
    {
        int LiczbaLap();
        string Procesor();
    }

}
