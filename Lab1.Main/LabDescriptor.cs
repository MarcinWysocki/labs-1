﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ISSak);
        
        public static Type ISub1 = typeof(IKot);
        public static Type Impl1 = typeof(Kot);
        
        public static Type ISub2 = typeof(IPies);
        public static Type Impl2 = typeof(Pies);
        
        
        public static string baseMethod = "Nazwa";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Miaucz";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Szczekaj";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "ListaZwierzakow";
        public static string collectionConsumerMethod = "JakSieNazywasz";

        #endregion

        #region P3

        public static Type IOther = typeof(IRobot);
        public static Type Impl3= typeof(KosmicznyKocur);

        public static string otherCommonMethod = "Nazwa";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
