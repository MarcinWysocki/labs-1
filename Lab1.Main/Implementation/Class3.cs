﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    class Pies : IPies
    {
        private string Imie;
        public Pies()
        {
            Imie = "Burek";
        }
        public string Nazwa()
        {
            return "Mam na imie " + this.Imie;
        }
        public string Szczekaj()
        {
            return "Hou hou";
        }
        public int LiczbaLap()
        {
            return 4;
        }
    }
	
	class Kot : IKot
    {
        private string Imie;
        public Kot()
        {
            Imie = "Mruczek";
        }
        public string Nazwa()
        {
            return "Mam na imie " + this.Imie;
        }
        public string Miaucz()
        {
            return "Miauuuuu";
        }
        public int LiczbaLap()
        {
            return 4;
        }
    }
	
	class KosmicznyKocur : IKot, IRobot
    {
        public int LiczbaLap()
        {
            return 2;
        }

        public string Procesor()
        {
            return "XEON E5-2600";
        }

        public string Miaucz()
        {
            return "Bzzz bzzzzz miaaaauuuu bzzzZ";
        }

        public string Nazwa()
        {
            return "Jestem kosmiczny kocur ex 2600";
        }
    }
	
	
}
